FROM ubuntu:16.04

RUN sed -i~orig -e 's/# deb-src/deb-src/' /etc/apt/sources.list \
    && apt-get -y update \
    && apt-get -y install build-essential bison flex pulseaudio aptitude curl cmake checkinstall git-core libpulse-dev \
    && apt-get -y build-dep chuck

RUN curl -L --location --retry 3 -o /tmp/chuck.tgz http://chuck.cs.princeton.edu/release/files/chuck-1.4.0.0.tgz

WORKDIR /tmp/

COPY data /tmp/

CMD ./build.sh
