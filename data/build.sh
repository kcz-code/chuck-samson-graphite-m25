#!/usr/bin/env bash

set -e

tar -xzf chuck.tgz
patch -s -p0 < sysex2.patch
cd chuck-1.4.0.0/src
make linux-jack
mv chuck /dest/chuck-jack
